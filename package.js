Package.describe({
  name: 'stupidtests',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.8');
  api.use('ecmascript');
  api.mainModule('stupidtests.js');
});

Package.onTest(function(api) {
  Npm.depends({
    chai: "4.2.0",
    sinon: "7.1.1",
    "sinon-chai": "3.2.0"
  });
  api.use('ecmascript');
  api.use('stupidtests');
  api.use("meteortesting:mocha");
  api.mainModule('stupidtests-tests.js');
});
